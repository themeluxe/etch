<?php get_template_part('templates/page', 'header'); ?>

<h1><?php esc_html_e('404', 'luxe-text-domain'); ?></h1>

<div class="h4"><?php esc_html_e('Sorry, but the page you were trying to view does not exist.', 'luxe-text-domain'); ?></div>

<a href="<?php echo get_home_url(); ?>" class="btn btn-default"><?php esc_html_e('Back Home', 'luxe-text-domain'); ?></a>