<?php 
use Luxe\Extras;
$author_id = get_the_author_meta( 'ID' ); 
?>
<div class="author-meta">
    <div class="row">
        <div class="col-sm-3 col-lg-2">
            <?php echo get_avatar( $author_id, 96, null, get_the_author_meta( 'display_name' ) ); ?> 
        </div>
        <div class="col-sm-9 col-lg-10">
            <h4><?php echo get_the_author_meta( 'display_name' ); ?></h4>
            <div class="author-description">
                <?php echo get_the_author_meta( 'description' ); ?>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="social-icons">
                        <ul>
                            <?php
                            $user_social_fields = Extras\user_social_fields();
                            foreach ($user_social_fields as $field) {
                                if ($url = get_user_meta( $author_id, '_user_' . $field . '_url', true )) {
                                    echo '<li><a href="' . $url . '" target="_blank"><i class="icon ion-social-' . $field .'"></i></a></li>';
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>