<?php use Luxe\Header; ?>
<?php do_action('luxe_before_header'); ?>
<?php get_template_part('templates/headers/' . get_theme_mod('header_style', 'default')); ?>
<?php do_action('luxe_after_header'); ?>
<div id="sidebar-off-canvas" class="sidebar off-canvas off-canvas-right sidebar-off-canvas" data-move-x="100%" data-move-y="" data-active-class="sidebar-offcanvas-active" data-content-push="0px">
    <?php dynamic_sidebar('sidebar-off-canvas'); ?>
</div>