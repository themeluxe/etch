<?php the_content(); ?>
<?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . esc_html__('Pages:', 'luxe-text-domain'), 'after' => '</p></nav>')); ?>
<?php get_template_part('templates/comments'); ?>
