<?php use Luxe\Extras; ?>
<?php 
    global $image_size;
    $image_size = isset($image_size) ? $image_size : 'etch_testimonial'; 
?>
<div class="col-sm-12">
    <article <?php post_class(''); ?>>
        <header>
            <?php if (has_post_thumbnail()) { ?>
                <div class="featured-content">
                    <?php the_post_thumbnail( $image_size ); ?>
                </div>
            <?php } ?>
            <h2 class="entry-title h3"><?php the_title(); ?></h2>
        </header>
        <div class="entry-summary">
            <?php the_content(); ?>
        </div>
    </article>
</div>