<?php use Luxe\Elements; ?>
<div class="hidden-md hidden-lg">
  <?php global $offcanvas_position; $offcanvas_position = 'left'; ?>
  <?php get_template_part( 'templates/headers/offcanvas-nav' ); ?>
</div>
<header class="banner header-left-align" id="header">
  <div class="container">
    <div class="pull-left">
        <div class="pull-left">
          <?php get_template_part( 'templates/headers/brand' ); ?>
        </div>
        <nav class="nav-primary pull-left hidden-xs hidden-sm">
          <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'navbar-nav nav'));
          endif;
          ?>
        </nav>
        <div class="hidden-md hidden-lg">
          <?php echo Elements\nav_button(); ?>
        </div>
    </div>
    <div class="pull-right">
      <?php echo Elements\header_buttons(); ?>
    </div>
  </div>
</header>
