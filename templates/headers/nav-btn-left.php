<?php use Luxe\Elements; ?>
<?php global $offcanvas_position; $offcanvas_position = 'left'; ?>
<?php get_template_part( 'templates/headers/offcanvas-nav' ); ?>
<header class="banner header-menu-btn-left" id="header">
    <div class="container">
        <?php get_template_part( 'templates/headers/brand' ); ?>
        <div class="pull-left">
            <?php echo Elements\nav_button(); ?>
        </div>
        <?php dynamic_sidebar('header-buttons'); ?>
        <?php echo Elements\header_buttons(); ?>
    </div>
</header>
