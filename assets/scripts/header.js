(function($) {
    "use strict";

	/**
	 * Global variables
	 */
	 var body = $( 'body' ),
	 	header = $('#header'),
     	preloader = $('#preloader');

	/**
	  * Page Loading Fffects
	  */
	$( document ).on( 'pageChange', function(event, url) {
		var translateHeaderDistance = '-100%';
		if(theme.headerPosition === 'fixed-bottom') {
			translateHeaderDistance = '100%';
		}
  		header
  			.velocity({ translateY: translateHeaderDistance },{ delay: 0, duration: 300, queue: false});
  		preloader
  			.velocity({ opacity: 1 }, { visibility: 'visible', delay: 300, duration: 500, complete: function(element) {
		    	window.location = url;					
			} });
	});

})(jQuery);