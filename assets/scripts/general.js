(function($) {
    "use strict";

	/**
	 * Counter
	 */	
	var counters = $('.counter-number');
	function startCounters() {
		counters.each(function() {
			if ($(this).inView() && !$(this).hasClass('animated')) {
				var options = {
				  useEasing : true, 
				  useGrouping : true, 
				  separator : $(this).data('separator'), 
				  decimal : $(this).data('decimal'), 
				  prefix : $(this).data('prefix'), 
				  suffix : $(this).data('suffix') 
				};
				var counter = new CountUp(this, 0, $(this).data('end-value'), $(this).data('decimal-places'), $(this).data('speed'), options);
				counter.start();
				$(this).addClass('animated');
			}
		});
	}
	$(document).ready(function() {
		startCounters();
	});
	$(window).scroll(function() {
		startCounters();
	});

	/**
	 * Google maps
	 */
	var maps = $('.google-map');
	function mapInit() {
		maps.each(function() {
			var fxn = window[$(this).data('fxn')];
			fxn();
		});
	}
	function lazyLoadGoogleMaps() {
	    var executed = false;
	    if (!executed) {
	        $.getScript("http://maps.google.com/maps/api/js?sensor=true")
	        .done(function (script, textStatus) {            
	            mapInit();
	        })
	        .fail(function (jqxhr, settings, ex) {
	        });
	    }
	}
	if (maps.length) {
		lazyLoadGoogleMaps();
	}

		/**
		 * Simple select boxes
		 */
		// $("select").simpleselect();
		// https://gist.github.com/NIA/4251971
		// $('select').each(function(i, e){
		//     if (($(e).data('convert') !== 'no')) {
		//         $(e).hide().wrap('<div class="btn-group select-group" id="select-group-' + $(e).attr('name') + '" />');
	 //            var selectGroup = $('#select-group-' + $(e).attr('name'));
	 //            var current = ($(e).val()) ? $(e).val(): '&nbsp;';
	 //            var textValue = '';

	 //            $(e).find('option').each(function(o,q) {
	 //                if(current === $(q).attr('value')){
	 //                    textValue = $(q).text();
	 //                }
	 //            });
	 //            selectGroup.prepend( '<button class="btn dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><span class="btn-text">' + textValue + ' </span><i class="icon ion-chevron-down"></i></button><ul class="dropdown-menu"></ul>');
	 //            $(e).find('option').each(function(o,q) {
	 //                selectGroup.find('.dropdown-menu').append('<li><a href="javascript:;" data-value="' + $(q).attr('value') + '" class="select-group-option">' + $(q).text() + '</a></li>');
	 //                if ($(q).attr('selected')) {
	 //                	selectGroup.find('.dropdown-menu li:eq(' + o + ')').click();
	 //                }
	 //            });

	 //            // Update on click
	 //            selectGroup.find('.dropdown-menu a').click(function() {
		//             $(e).val($(this).data('value')).change();
		//             selectGroup.find('.btn-text:eq(0)').html($(this).html());
	 //        	});
		//     }
		// });


	/**
	 * Portfolio loading animation
	 */
	 // $('a.portfolio-link').click(function(e) {
	 // 	e.preventDefault();
	 // 	var featuredContent = $(this).closest('article').find('.featured-content img:first');
	 // 	if (featuredContent.length) {
	 // 		var scrollTop = $(window).scrollTop(),
	 // 		featuredPosition = featuredContent.offset(),
	 // 		featuredWidth = featuredContent.width(),
	 // 		featuredHeight = featuredContent.height();
	 // 		var featuredContentClone = featuredContent.clone().hide().appendTo('body').attr('id', 'featured-content-clone');
	 // 		featuredContentClone.css({
	 // 			'position':'fixed',
	 // 			'top':featuredPosition.top - scrollTop, 
	 // 			'left':featuredPosition.left,
	 // 			'width': featuredWidth,
	 // 			'height': featuredHeight
	 // 		});
	 // 		featuredContentClone.show();
	 // 	}
	 // 	setTimeout(function() {
	 // 		var featuredPrimary = $('#featured-img-primary');
	 // 		featuredPosition = featuredPrimary.offset();
	 // 		featuredWidth = featuredPrimary.width();
	 // 		featuredHeight = featuredPrimary.height();
	 // 		featuredContentClone = $('#featured-content-clone');
	 // 		featuredContentClone.css({
	 // 			'top':featuredPosition.top, 
	 // 			'left':featuredPosition.left,
	 // 			'width': featuredWidth,
	 // 			'height': featuredHeight,
	 // 			'position': 'absolute'
	 // 		});

	 // 	}, 1000);
	 // 	setTimeout(function() {
	 // 		featuredContentClone.velocity({ opacity: '0', translateZ: 0},{ delay: 500, duration: 500, queue: false});
	 // 		$content
	 // 			.velocity({ opacity: '1', translateZ: 0},{ delay: 500, duration: 500, queue: false,
	 // 			complete: function() {
	 // 				featuredContentClone.remove();
	 // 			} 
	 // 		});
	 // 	}, 2000);
	 // });


// 		$.Velocity
// 		    .RegisterEffect("translateDown", {
// 		    	defaultDuration: 1,
// 		        calls: [ 
// 		            [ { translateY: '100%'}, 1]
// 		        ]
// 		    });
// $.Velocity
//     .RegisterEffect("translateUp.half", {
//     	defaultDuration: 1,
//         calls: [ 
//             [ { translateY: '-50%'}, 1]
//         ]
//     });

    // $.Velocity.RegisterEffect("transition.translateDown", {
    //     defaultDuration: 900,
    //     calls: [
    //         [ { translateY: [ 0, -100% ], translateZ: 0 } ]
    //     ]
    // });
    // $.Velocity
    //     .RegisterEffect("translateDown", {
    //     	defaultDuration: 1,
    //         calls: [ 
    //             [ { translateY: [ 0, -100% ], translateZ: 0 } ]
    //         ]
    //     });


    /**
     * Parallax backgrounds
     */
 //    var parallaxBgs = $('.parallax-bg');
 //    var win = $(window);
 //    var ticking = false;
 //    window.requestAnimFrame = (function(){
 //      return  window.requestAnimationFrame       ||
 //              window.webkitRequestAnimationFrame ||
 //              window.mozRequestAnimationFrame    ||
 //              function( callback ){
 //                window.setTimeout(callback, 1000 / 60);
 //              };
 //    })();
	// function updateParallax() {
 //    	var scrollTop = win.scrollTop();
	//     parallaxBgs.each(function() {
 //    		var parallaxStart = $(this).offset().top * 0.9;
	//     	if (scrollTop > parallaxStart) {
	//     		// var value = 200 * ( 1 - (position.top + $(this).height() - scrollTop) / win.height());
	//     		var value = (scrollTop - parallaxStart) / 2;
	//     		var img = $(this).find('div:first');
	//     		img.velocity({ translateY: -value, translateZ: 0},{ delay: 0, duration: 0, queue: false, easing: 'easeInSine' });
	//     	}
	//     });
	//     // Stop ticking
	//     ticking = false;
	// }
 //    if (parallaxBgs.length) {
 //    	var lastScrollY = 0;
 //    	var requestTick = function() {
	// 		if (!ticking) {
	// 			window.requestAnimationFrame(updateParallax);
	// 			ticking = true;
	// 		}
 //    	};
 //    	var doScroll = function() {
	// 		lastScrollY = window.pageYOffset;
	// 		requestTick();
 //    	};
	//     window.addEventListener('scroll', doScroll, false);
	// }

	/**	
	 *	Scroll to fixed
	 */

	// $('.scroll-to-fixed').scrollToFixed({
	//     marginTop: function() {
	//     	if( theme.headerPosition === 'fixed-bottom' ) {
	//     		return 40;
	//     	} else {
	// 	    	return $('header.banner').outerHeight() + 40;    		
	//     	}
	//     },
	//     limit: 0,	
	//     removeOffsets: true
	// });

})(jQuery);