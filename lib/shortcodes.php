<?php

namespace Luxe\Shortcodes;


if (class_exists('Luxe_Shortcodes')) {
    /**
     * Add shortcodes
     */
    $shortcodes = array(
        'vc_overrides',
        'vc_ionicons',
        'grid',
        'pricing-boxes',
        'slider',
        'slide',
        'map',
        'counter',
        'info-box',
        'modal',
        'accordion',
        'accordion_section',
        'icon',
        'link',
        'button',
        'separator',
        'progress-bar',
        'unordered-list',
        'animation',
        'social',
        'social_item',
        'typewriter',
        'hover-box',
    );

    new \Luxe_Shortcodes($shortcodes);
}