<?php

/**
 * Recommended Plugins
 */
function etch_register_required_plugins() {
	/*
	 * Array of plugin arrays
	 */
	$plugins = array(

		array(
			'name'               => esc_html__('ThemeLuxe Shortcodes and Custom Post Types', 'luxe-text-domain'),
			'slug'               => 'themeluxe-shortcodes',
			'source'             => get_template_directory() . '/lib/plugins/themeluxe-shortcodes.zip',
			'required'           => true,
			'version'            => '',
			'force_activation'   => false,
			'force_deactivation' => false,
			'external_url'       => '',
			'is_callable'        => '',
		),

		array(
			'name'               => esc_html__('Visual Composer', 'luxe-text-domain'),
			'slug'               => 'js_composer',
			'source'             => get_template_directory() . '/lib/plugins/js_composer.zip',
			'required'           => true,
			'version'            => '',
			'force_activation'   => false,
			'force_deactivation' => false,
			'external_url'       => '',
			'is_callable'        => '',
		),

		array(
			'name'      => esc_html__('Kirki Toolkit', 'luxe-text-domain'),
			'slug'      => 'kirki',
			'required'  => true,
		),
		array(
			'name'      => esc_html__('WooCommerce', 'luxe-text-domain'),
			'slug'      => 'woocommerce',
			'required'  => false,
		),
		array(
			'name'      => esc_html__('WC Variations Radio Buttons', 'luxe-text-domain'),
			'slug'      => 'wc-variations-radio-buttons',
			'required'  => false,
		),
		array(
			'name'      => esc_html__('Contact Form 7', 'luxe-text-domain'),
			'slug'      => 'contact-form-7',
			'required'  => false,
		),
		
	);

	/*
	 * Configuration settings
	 */
	$config = array(
		'id'           => 'luxe_tgmpa',
		'default_path' => '',                      
		'menu'         => 'tgmpa-install-plugins',
		'parent_slug'  => 'themes.php',
		'capability'   => 'edit_theme_options',
		'has_notices'  => true,
		'dismissable'  => true,
		'dismiss_msg'  => '',
		'is_automatic' => true,
		'message'      => '',
		/*
		'strings'      => array(
			'page_title'                      => esc_html__( 'Install Required Plugins', 'theme-slug' ),
			'menu_title'                      => esc_html__( 'Install Plugins', 'theme-slug' ),
			// <snip>...</snip>
			'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
		)
		*/
	);

	tgmpa( $plugins, $config );

}
add_action( 'tgmpa_register', 'etch_register_required_plugins' );
