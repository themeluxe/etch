<?php

use Luxe\Helper;

if (Helper\is_woocommerce_active()) {

    LuxeOption::add_section( 'woocommerce', array(
        'title'          => esc_attr__( 'Woocommerce', 'luxe-text-domain' ),
        'priority'       => 1,
        'capability'     => 'edit_theme_options',
    ) );

    LuxeOption::add_field( 'luxe_options', array(
        'type'        => 'select',
        'settings'    => 'shop_header_scheme',
        'label'       => esc_attr__( 'Single Product Page Header Scheme', 'luxe-text-domain' ),
        'description' => esc_attr__( 'Select default header used for single product pages.', 'luxe-text-domain' ),
        'section'     => 'woocommerce',
        'default'     => 'header-dark-active',
        'priority'    => 10,
        'choices'     => array(
            'header-dark-active' => esc_attr__( 'Dark Header', 'luxe-text-domain' ),
            'header-light-active'   => esc_attr__( 'Light Header', 'luxe-text-domain' ),
        ),
    ) );
    LuxeOption::add_field( 'luxe_options', array(
        'type'        => 'switch',
        'settings'    => 'open_cart_on_ajax_add',
        'label'       => esc_attr__( 'Open Cart on Add', 'luxe-text-domain' ),
        'description' => esc_attr__( 'Open cart sidebar when item is added through AJAX.', 'luxe-text-domain' ),
        'section'     => 'woocommerce',
        'default'     => true,
        'priority'    => 10,
    ) );
    LuxeOption::add_field( 'luxe_options', array(
        'type'        => 'switch',
        'settings'    => 'shop_hide_kitchen_sink',
        'label'       => esc_attr__( 'Hide Kitchen Sink', 'luxe-text-domain' ),
        'description' => esc_attr__( 'Hide description, additional information, and reviews beneath products on single product pages.', 'luxe-text-domain' ),
        'section'     => 'woocommerce',
        'default'     => false,
        'priority'    => 10,
    ) );

}