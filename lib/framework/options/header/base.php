<?php

LuxeOption::add_panel( 'header', array(
    'title'          => esc_attr__( 'Header & Navigation', 'luxe-text-domain' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );
LuxeOption::add_section( 'header_base', array(
    'title'          => esc_attr__( 'Base Header', 'luxe-text-domain' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'header'
) );

/**
 * Basic header styles
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'radio-image',
    'settings'    => 'header_style',
    'label'       => esc_attr__( 'Header Style', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Pick the style and layout of your header.', 'luxe-text-domain' ),
    'help'        => esc_attr__( '', 'luxe-text-domain' ),
    'section'     => 'header_base',
    'default'     => 'none',
    'priority'    => 10,
        'choices'     => array(
            // 'default'   => 'Default',
            // 'left-align' => 'Left Align Navigation',
            // 'centered-nav' => 'Centered Navigation',
            // 'nav-btn-left'  => 'Nav Button Left',
            // 'nav-btn-right'  => 'Nav Button Right',
            // 'centered-logo-and-nav'  => 'Centered Logo and Navigation',
            // 'nav-overlay'  => 'Navigation Overlay',
            'default'   => get_template_directory_uri() . '/dist/images/header-styles/default.jpg',
            'left-align' => get_template_directory_uri() . '/dist/images/header-styles/left-align.jpg',
            'centered-nav' => get_template_directory_uri() . '/dist/images/header-styles/centered-nav.jpg',
            'nav-btn-left'  => get_template_directory_uri() . '/dist/images/header-styles/nav-btn-left.jpg',
            'nav-btn-right'  => get_template_directory_uri() . '/dist/images/header-styles/nav-btn-right.jpg',
            'centered-logo-and-nav'  => get_template_directory_uri() . '/dist/images/header-styles/centered-logo-and-nav.jpg',
            'nav-overlay'  => get_template_directory_uri() . '/dist/images/header-styles/nav-overlay.jpg',
            
        ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'select',
    'settings'    => 'header_scheme',
    'label'       => esc_attr__( 'Default Header Scheme', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Select default header used for pages.  This can be changed on each individual page.', 'luxe-text-domain' ),
    'section'     => 'header_base',
    'default'     => 'header-dark-active',
    'priority'    => 10,
    'choices'     => array(
        'header-dark-active' => esc_attr__( 'Dark Header', 'luxe-text-domain' ),
        'header-light-active'   => esc_attr__( 'Light Header', 'luxe-text-domain' ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'dimension',
    'settings'    => 'header_container_width',
    'label'       => esc_attr__( 'Maximum Container Width', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Controls how wide your main header is on larger screens.', 'luxe-text-domain' ),
    'section'     => 'header_base',
    'default'     => '1170px',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'header.banner .container',
            'property' => 'max-width',
            'media_query' => '@media (min-width: 1200px)'
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => 'header.banner .container',
            'property' => 'max-width',
            'function' => 'css',
        ),
    ),
    'choices' => array(
        'units' => array( 'px', '%' )
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'radio-buttonset',
    'settings'    => 'header_position',
    'label'       => esc_attr__( 'Header Position', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Choose text transform for navigation.', 'luxe-text-domain' ),
    'help'        => esc_attr__( '', 'luxe-text-domain' ),
    'section'     => 'header_base',
    'default'     => 'absolute',
    'priority'    => 10,
    'choices'     => array(
        'static'  => 'Static',
        'fixed'   => 'Fixed to top',
        'fixed-bottom'   => 'Fixed to Bottom',
        'absolute' => 'On top of content',
    ),
    'output' => array(
        array(
            'element' => 'header.banner',
            'function' => 'css',
            'property' => 'position',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'select',
    'settings'    => 'nav_button',
    'label'       => esc_attr__( 'Navigation Button', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Select the style of your navigation / hamburger icon.  This is used in certain header styles and for your mobile navigation.', 'luxe-text-domain' ),
    'section'     => 'header_base',
    'default'     => 'icon',
    'priority'    => 10,
    'choices'     => array(
        'icon'   => esc_attr__( 'Icon', 'luxe-text-domain' ),
        'text' => esc_attr__( 'Text', 'luxe-text-domain' ),
        'icon_and_text'      => esc_attr__( 'Icon and Text', 'luxe-text-domain' ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'text',
    'settings'    => 'nav_button_text',
    'label'       => esc_attr__( 'Navigation Button Text', 'luxe-text-domain' ),
    'description' => esc_attr__( 'The text used as your navigation button.', 'luxe-text-domain' ),
    'section'     => 'header_base',
    'default'     => 'Menu',
    'priority'    => 10,
    'required'    => array(
        array(
            'setting'  => 'nav_button',
            'operator' => '!=',
            'value'    => 'icon',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'header_height',
    'label'       => esc_attr__( 'Header Height', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Control the height of your header in pixels.', 'luxe-text-domain' ),
    'help'        => esc_attr__( '', 'luxe-text-domain' ),
    'section'     => 'header_base',
    'default'     => '80',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'header.banner, header.banner .navbar-nav>li, header.banner .brand-wrap, .nav-btn-wrap',
            'property' => 'height',
            'units'    => 'px',
        ),
        array(
            'element'  => 'body.single-portfolio #content.container, body.single-product #content.container',
            'property' => 'padding-top',
            'units'    => 'px',
        ),
        array(
            'element'  => 'body.header-position-fixed-bottom',
            'property' => 'padding-bottom',
            'units'    => 'px',
        ),
        array(
            'element'  => '.main>.outer-container:first-child>.vc_row',
            'property' => 'padding-top',
            'units'    => 'px',
        ),
        array(
            'element'  => '.brand-text',
            'property' => 'line-height',
            'units'    => 'px',
        ),
        array(
            'element'  => '.navbar-brand>img',
            'property' => 'max-height',
            'units'    => 'px',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => 'header.banner, header.banner:not(.header-centered-logo-and-nav) .navbar-nav>li, header.banner .brand-wrap, .nav-btn-wrap',
            'property' => 'height',
            'units'    => 'px',
            'function' => 'css',
        ),
        array(
            'element'  => 'body.single-portfolio #content.container, body.single-product #content.container',
            'property' => 'padding-top',
            'units'    => 'px',
            'function' => 'css',
        ),
        array(
            'element'  => 'body.header-position-fixed-bottom',
            'property' => 'padding-bottom',
            'units'    => 'px',
            'function' => 'css',
        ),
        array(
            'element'  => '.brand-text',
            'property' => 'line-height',
            'units'    => 'px',
        ),
        array(
            'element'  => '.navbar-brand>img',
            'property' => 'max-height',
            'units'    => 'px',
            'function' => 'css',
        ),
    ),
    'choices'      => array(
        'min'  => 50,
        'max'  => 300,
        'step' => 1,
    )
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'switch',
    'settings'    => 'brand_hide',
    'label'       => esc_attr__( 'Hide Branding', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Hides the logo or site name from your header.', 'luxe-text-domain' ),
    // 'help'        => esc_attr__( '', 'luxe-text-domain' ),
    'section'     => 'header_base',
    'default'     => false,
    'priority'    => 10,
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'logo_padding_top',
    'label'       => esc_attr__( 'Logo Top Padding', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Control the padding in pixels above your logo.', 'luxe-text-domain' ),
    'help'        => esc_attr__( '', 'luxe-text-domain' ),
    'section'     => 'header_base',
    'default'     => '15',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.navbar-brand>span, .navbar-brand>img',
            'property' => 'padding-top',
            'units'    => 'px',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => '.navbar-brand>span, .navbar-brand>img',
            'property' => 'padding-top',
            'units'    => 'px',
            'function' => 'css',
        ),
    ),
    'choices'      => array(
        'min'  => 0,
        'max'  => 80,
        'step' => 1,
    )
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'logo_padding_bottom',
    'label'       => esc_attr__( 'Logo Bottom Padding', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Control the padding in pixels below your logo.', 'luxe-text-domain' ),
    'help'        => esc_attr__( '', 'luxe-text-domain' ),
    'section'     => 'header_base',
    'default'     => '0',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.navbar-brand>span, .navbar-brand>img',
            'property' => 'padding-bottom',
            'units'    => 'px',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => '.navbar-brand>span, .navbar-brand>img',
            'property' => 'padding-bottom',
            'units'    => 'px',
            'function' => 'css',
        ),
    ),
    'choices'      => array(
        'min'  => 0,
        'max'  => 80,
        'step' => 1,
    )
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'typography',
    'settings'    => 'header_typography',
    'label'       => esc_attr__( 'Header Typography', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Typography for other elements in your header.', 'luxe-text-domain' ),
    'help'        => esc_attr__( '', 'luxe-text-domain' ),
    'section'     => 'header_base',
    'default'     => array(
        'font-style'     => array( 'bold', 'italic' ),
        'font-family'    => 'Roboto',
        'font-size'      => '14',
        'font-weight'    => '400',
        'line-height'    => '1',
        'letter-spacing' => '0',
        'text-transform' => 'none',
    ),
    'priority'    => 10,
    'choices'     => array(
        'font-style'     => true,
        'font-family'    => true,
        'font-size'      => true,
        'font-weight'    => true,
        'line-height'    => false,
        'letter-spacing' => true,
        'units'          => array( 'px', 'rem' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => 'header.banner',
        ),
    ),
) );