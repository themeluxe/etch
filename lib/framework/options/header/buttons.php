<?php

LuxeOption::add_section( 'header_buttons', array(
    'title'          => esc_attr__( 'Header Buttons', 'luxe-text-domain' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'header',
) );

/**
 * Header Buttons
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'switch',
    'settings'    => 'header_cart',
    'label'       => esc_attr__( 'Cart Button', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Adds a cart button with the cart count.', 'luxe-text-domain' ),
    'section'     => 'header_buttons',
    'default'     => false,
    'priority'    => 10,
    'transport'   => 'refresh'
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'switch',
    'settings'    => 'header_search',
    'label'       => esc_attr__( 'Search Button', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Adds an option to search the site from the header.', 'luxe-text-domain' ),
    'section'     => 'header_buttons',
    'default'     => false,
    'priority'    => 10,
    'transport'   => 'refresh'
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'repeater',
    'label'       => esc_attr__( 'Header Buttons', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Add or remove the buttons you\'d like to use in your header button area.  This position may vary depending on the header style.', 'luxe-text-domain' ),
    'help'        => esc_attr__( 'This is a good place to put links, cart buttons, or a call to action.', 'luxe-text-domain' ),
    'section'     => 'header_buttons',
    'priority'    => 10,
    'settings'    => 'header_buttons',
    'default'     => array(
        array(
            'button_text' => esc_attr__( 'Cart', 'luxe-text-domain' ),
            'button_url'  => '#',
        ),
    ),
    'fields' => array(
        'text' => array(
            'type'        => 'text',
            'label'       => esc_attr__( 'Text', 'luxe-text-domain' ),
            'description' => esc_attr__( 'This can be text, a shortcode, or HTML.', 'luxe-text-domain' ),
            'sanitize_callback' => array( 'LuxeOption', 'unfiltered' ),
        ),
        'action' => array(
            'type'        => 'select',
            'label'       => esc_attr__( 'Action', 'luxe-text-domain' ),
            'description' => esc_attr__( 'Set what will happen when the button is clicked.', 'luxe-text-domain' ),
            'choices'     => array(
                'link' => esc_attr__( 'Link To', 'luxe-text-domain' ),
                'offcanvas_widget'   => esc_attr__( 'Open Offcanvas Widget', 'luxe-text-domain' ),
                'modal' => esc_attr__( 'Open Modal Window', 'luxe-text-domain' ),
            ),
        ),
        'url' => array(
            'type'        => 'text',
            'label'       => esc_attr__( 'URL', 'luxe-text-domain' ),
            'description' => esc_attr__( 'This will be the link URL', 'luxe-text-domain' ),
            'default'     => '',
            'required'    => array(
                array(
                    'setting'  => 'target',
                    'operator' => '==',
                    'value'    => 'link',
                ),
            ),
            'sanitize_callback' => 'esc_url_raw',
        ),
        'style' => array(
            'type'        => 'select',
            'label'       => esc_attr__( 'Link Style', 'luxe-text-domain' ),
            'description' => esc_attr__( 'How your link/button will be portrayed.', 'luxe-text-domain' ),
            'choices'     => array(
                ''   => esc_attr__( 'Regular Link', 'luxe-text-domain' ),
                'btn btn-default btn-sm' => esc_attr__( 'Default Button', 'luxe-text-domain' ),
                'btn btn-primary btn-sm' => esc_attr__( 'Primary Button', 'luxe-text-domain' ),
            ),
        ),
    )
) );