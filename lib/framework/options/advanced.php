<?php

LuxeOption::add_section( 'advanced', array(
    'title'          => esc_attr__( 'Advanced', 'luxe-text-domain' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );

/**
 * Advanced
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'code',
    'settings'    => 'custom_css',
    'label'       => esc_attr__( 'Custom CSS', 'luxe-text-domain' ),
    // 'help'        => esc_attr__( '', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Add your custom CSS here to override theme settings.', 'luxe-text-domain' ),
    'section'     => 'advanced',
    'default'     => '',
    'priority'    => 10,
    'choices'     => array(
        'language' => 'css',
        'theme'    => 'monokai',
        'height'   => 250,
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'code',
    'settings'    => 'custom_js',
    'label'       => esc_attr__( 'Custom Javascript', 'luxe-text-domain' ),
    // 'help'        => esc_attr__( '', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Add any custom javascript here.  Do not include the <\script\> tags.', 'luxe-text-domain' ),
    'section'     => 'advanced',
    'default'     => '',
    'priority'    => 10,
    'choices'     => array(
        'language' => 'javascript',
        'theme'    => 'monokai',
        'height'   => 250,
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'code',
    'settings'    => 'custom_head_scripts',
    'label'       => esc_attr__( 'Custom Head Scripts', 'luxe-text-domain' ),
    'description'     => esc_attr__( 'Add custom scripts or styles in your head.  You can include the <\script\> tags.  This is a good place for TypeKit scripts, Google Analytics, or other third party scripts/styles.', 'luxe-text-domain' ),
    'section'     => 'advanced',
    'default'     => '',
    'priority'    => 10,
    'choices'     => array(
        'language' => 'html',
        'theme'    => 'monokai',
        'height'   => 250,
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'switch',
    'settings'    => 'visual_composer',
    'label'       => esc_attr__( 'Visual Composer Scripts', 'luxe-text-domain' ),
    'help'        => esc_attr__( 'Turn on or off the visual composer scripts and styles.  Turning this off will remove some features included with Visual Composer but may drastically speed up your site by reducing the amount of files loaded.', 'luxe-text-domain' ),
    'section'     => 'advanced',
    'default'     => false,
    'priority'    => 10,
) );