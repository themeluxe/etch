<?php

LuxeOption::add_section( 'sidebars', array(
    'title'          => esc_attr__( 'Sidebars', 'luxe-text-domain' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'body',
) );

/**
 * Sidebars
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'switch',
    'settings'    => 'sidebar_page',
    'label'       => esc_attr__( 'Page Sidebars', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Turn on or off all page sidebars by default.', 'luxe-text-domain' ),
    'help'        => esc_attr__( 'This can be overriden on each individual page.', 'luxe-text-domain' ),
    'section'     => 'sidebars',
    'default'     => false,
    'priority'    => 10,
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'switch',
    'settings'    => 'sidebar_single',
    'label'       => esc_attr__( 'Single Post Sidebars', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Turn on or off all post sidebars by default.', 'luxe-text-domain' ),
    'help'        => esc_attr__( 'This can be overriden on each individual page.', 'luxe-text-domain' ),
    'section'     => 'sidebars',
    'default'     => false,
    'priority'    => 10,
) );