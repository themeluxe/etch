<?php

LuxeOption::add_section( 'buttons_primary', array(
    'title'          => esc_attr__( 'Primary Buttons', 'luxe-text-domain' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'body',
) );

$buttons = array();
$buttons[] = '.button';
$buttons[] = 'input[type=submit]';
$buttons[] = '.comment-form input[type=submit]';
$buttons[] = '.btn.btn-primary';
$buttons[] = '.vc_btn3.vc_btn3-style-theme-primary';
$buttons[] = '.woocommerce a.button';
$buttons[] = '.woocommerce input.button';
$buttons[] = '.woocommerce button.button.alt:disabled[disabled]';
$buttons[] = '.dummy-keep-at-end-btn';

/**
 * Primary Buttons
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'button_primary_bg_color',
    'label'       => esc_attr__( 'Button Background Color', 'luxe-text-domain' ),
    'section'     => 'buttons_primary',
    'default'     => '#ffffff',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'background-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'background-color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'button_primary_bg_color_hover',
    'label'       => esc_attr__( 'Button Hover Background Color', 'luxe-text-domain' ),
    'section'     => 'buttons_primary',
    'default'     => '#ffffff',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(':hover, ', $buttons),
            'property' => 'background-color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'button_primary_text_color',
    'label'       => esc_attr__( 'Button Text Color', 'luxe-text-domain' ),
    'section'     => 'buttons_primary',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'button_primary_text_color_hover',
    'label'       => esc_attr__( 'Button Hover Text Color', 'luxe-text-domain' ),
    'section'     => 'buttons_primary',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(':hover, ', $buttons),
            'property' => 'color',
            'suffix' => ' !important'
        ),
    ),
    'transport'   => 'postMessage',
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'button_primary_border_color',
    'label'       => esc_attr__( 'Button Border Color', 'luxe-text-domain' ),
    'section'     => 'buttons_primary',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'border-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'border-color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'button_primary_border_color_hover',
    'label'       => esc_attr__( 'Button Hover Border Color', 'luxe-text-domain' ),
    'section'     => 'buttons_primary',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(':hover, ', $buttons),
            'property' => 'border-color',
        ),
    ),
) );
