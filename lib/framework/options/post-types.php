<?php

LuxeOption::add_section( 'portfolio', array(
    'title'          => esc_attr__( 'Portfolio', 'luxe-text-domain' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );
LuxeOption::add_section( 'posts', array(
    'title'          => esc_attr__( 'Blog Posts', 'luxe-text-domain' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );

/**
 * Portfolio
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'text',
    'settings'    => 'portfolio_slug',
    'label'       => esc_attr__( 'Portfolio URL Slug', 'luxe-text-domain' ),
    // 'help'        => esc_attr__( 'This is a tooltip', 'luxe-text-domain' ),
    'description' => esc_attr__( 'The slug used in your portfolio URL for items.  http://yourdomain.com/<span style="color:red;"">portfolio-item</span>/your-item-name', 'luxe-text-domain' ),
    'default'     => 'portfolio-item',
    'section'     => 'portfolio',
    'default'     => '',
    'priority'    => 10,
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'text',
    'settings'    => 'portfolio_link',
    'label'       => esc_attr__( 'Portfolio Back Link', 'luxe-text-domain' ),
    // 'help'        => esc_attr__( 'This is a tooltip', 'luxe-text-domain' ),
    'description' => esc_attr__( 'The URL used for the back to portfolio button on your single portfolio item pages.', 'luxe-text-domain' ),
    'default'     => '',
    'section'     => 'portfolio',
    'default'     => '',
    'priority'    => 10,
) );

/**
 * Posts
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'radio-buttonset',
    'settings'    => 'single_post_style',
    'label'       => esc_attr__( 'Single Post Style', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Pick the style and layout of your individual blog posts.', 'luxe-text-domain' ),
    'section'     => 'posts',
    'default'     => 'default',
    'priority'    => 10,
    'choices'     => array(
        'default'   => 'Default',
        'top'  => 'Featured top',
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'text',
    'settings'    => 'blog_link',
    'label'       => esc_attr__( 'Blog Back Link', 'luxe-text-domain' ),
    // 'help'        => esc_attr__( 'This is a tooltip', 'luxe-text-domain' ),
    'description' => esc_attr__( 'The URL used for the back to blog button on your single post item pages.', 'luxe-text-domain' ),
    'default'     => '',
    'section'     => 'posts',
    'default'     => '',
    'priority'    => 10,
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'select',
    'settings'    => 'blog_header_scheme',
    'label'       => esc_attr__( 'Blog Header Scheme', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Select default header used for blog pages and archive pages.', 'luxe-text-domain' ),
    'section'     => 'posts',
    'default'     => 'header-dark-active',
    'priority'    => 10,
    'choices'     => array(
        'header-dark-active' => esc_attr__( 'Dark Header', 'luxe-text-domain' ),
        'header-light-active'   => esc_attr__( 'Light Header', 'luxe-text-domain' ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'blog_page_header_typography_color',
    'label'       => esc_attr__( 'Archive Page Header Font Color', 'luxe-text-domain' ),
    'section'     => 'posts',
    'default'     => '#1e1e1e',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'body.archive .page-header, body.archive .page-header a, body.archive .page-header h1, body.archive .page-header .subtitle, body.archive .page-header div',
            'property' => 'color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => 'body.archive .page-header, body.archive .page-header a, body.archive .page-header h1, body.archive .page-header .subtitle, body.archive .page-header div',
            'property' => 'color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'blog_page_header_bg_color',
    'label'       => esc_attr__( 'Archive Page Header Background Color', 'luxe-text-domain' ),
    'section'     => 'posts',
    'default'     => '#fff',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'body.archive .page-header',
            'property' => 'background-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => 'body.archive .page-header',
            'property' => 'background-color',
            'function' => 'css',
        ),
    ),
) );
// LuxeOption::add_field( 'luxe_options', array(
//     'type'        => 'select',
//     'settings'    => 'blog_columns',
//     'label'       => esc_attr__( 'Blog Columns', 'luxe-text-domain' ),
//     'description' => esc_attr__( 'Number of columns shown on your blog index page.', 'luxe-text-domain' ),
//     'section'     => 'posts',
//     'default'     => '1',
//     'priority'    => 10,
//     'choices'     => array(
//         '1' => esc_attr__( '1', 'luxe-text-domain' ),
//         '2' => esc_attr__( '2', 'luxe-text-domain' ),
//         '3' => esc_attr__( '3', 'luxe-text-domain' ),
//         '4' => esc_attr__( '4', 'luxe-text-domain' ),
//     ),
// ) );