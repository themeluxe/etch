<?php

LuxeOption::add_section( 'preloader', array(
    'title'          => esc_attr__( 'Preloader', 'luxe-text-domain' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );

/**
 * General page loading
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'switch',
    'settings'    => 'preloader',
    'label'       => esc_attr__( 'Preloader', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Shows a preloader on the screen until all your content is loaded.', 'luxe-text-domain' ),
    'help'        => esc_attr__( 'This feature will be disabled temporarily while using the customizer.', 'luxe-text-domain' ),
    'section'     => 'preloader',
    'default'     => false,
    'priority'    => 10,
) );

LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'preloader_bg_color',
    'label'       => esc_attr__( 'Loading Background Color', 'luxe-text-domain' ),
    'description' => esc_attr__( 'The background color of the overlay between page loads.', 'luxe-text-domain' ),
    'section'     => 'preloader',
    'default'     => '#fff',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.preloader',
            'property' => 'background-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.preloader',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'editor',
    'settings'    => 'preloader_content',
    'label'       => esc_attr__( 'Preloader Content', 'luxe-text-domain' ),
    'default'     => esc_attr__( 'Content displayed in your loading area.  You can use text, a logo, or any other content you like here.', 'luxe-text-domain' ),
    'section'     => 'preloader',
    'default'     => '',
    'priority'    => 10,
) );