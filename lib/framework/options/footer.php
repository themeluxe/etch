<?php


LuxeOption::add_section( 'footer', array(
    'title'          => esc_attr__( 'Footer', 'luxe-text-domain' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );

LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'footer_columns',
    'label'       => esc_attr__( 'Footer Columns', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Number of footer column widgets to display above your footer copy area.', 'luxe-text-domain' ),
    'section'     => 'footer',
    'default'     => '3',
    'priority'    => 10,
    'choices'      => array(
        'min'  => 0,
        'max'  => 4,
        'step' => 1,
    )
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'switch',
    'settings'    => 'footer_fixed',
    'label'       => esc_attr__( 'Fixed Footer', 'luxe-text-domain' ),
    'description' => esc_attr__( 'If turned on your footer will slide out from behind your content when scrolled down.', 'luxe-text-domain' ),
    'section'     => 'footer',
    'default'     => true,
    'priority'    => 10,
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'switch',
    'settings'    => 'footer_copy',
    'label'       => esc_attr__( 'Footer Copy', 'luxe-text-domain' ),
    'description' => esc_attr__( 'Turn on or off the footer copy beneath your footer widgets.', 'luxe-text-domain' ),
    'section'     => 'footer',
    'default'     => true,
    'priority'    => 10,
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'footer_headings_typography_color',
    'label'       => esc_attr__( 'Footer Headings Font Color', 'luxe-text-domain' ),
    'section'     => 'footer',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'footer.content-info h1, footer.content-info .h1, footer.content-info h2, footer.content-info .h2, footer.content-info h3, footer.content-info .h3, footer.content-info h4, footer.content-info .h4, footer.content-info h5, footer.content-info .h5',
            'property' => 'color',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => 'footer.content-info h1, footer.content-info .h1, footer.content-info h2, footer.content-info .h2, footer.content-info h3, footer.content-info .h3, footer.content-info h4, footer.content-info .h4, footer.content-info h5, footer.content-info .h5',
            'property' => 'color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'footer_typography_color',
    'label'       => esc_attr__( 'Footer Font Color', 'luxe-text-domain' ),
    'section'     => 'footer',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'footer.content-info, footer.content-info p',
            'property' => 'color',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => 'footer.content-info, footer.content-info p',
            'property' => 'color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'footer_links_color',
    'label'       => esc_attr__( 'Footer Links Color', 'luxe-text-domain' ),
    'section'     => 'footer',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'footer.content-info a',
            'property' => 'color',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => 'footer.content-info a',
            'property' => 'color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'footer_links_color_hover',
    'label'       => esc_attr__( 'Footer Links Hover Color', 'luxe-text-domain' ),
    'section'     => 'footer',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'footer.content-info a:hover',
            'property' => 'color',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => 'footer.content-info a:hover',
            'property' => 'color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'footer_bg_color',
    'label'       => esc_attr__( 'Footer Background Color', 'luxe-text-domain' ),
    'section'     => 'footer',
    'default'     => '#ffffff',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'footer.content-info',
            'property' => 'background-color',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => 'footer.content-info',
            'property' => 'background-color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'footer_copy_bg_color',
    'label'       => esc_attr__( 'Footer Copy Background Color', 'luxe-text-domain' ),
    'section'     => 'footer',
    'default'     => '#ffffff',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'footer.content-info .footer-wrap',
            'property' => 'background-color',
            'units'    => 'px',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => 'footer.content-info .footer-wrap',
            'property' => 'background-color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'footer_widgets_padding',
    'label'       => esc_attr__( 'Footer Widgets Padding', 'luxe-text-domain' ),
    'description' => esc_attr__( 'The space above and below your footer widgets.', 'luxe-text-domain' ),
    'section'     => 'footer',
    'default'     => '100',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.footer-widgets-wrap',
            'property' => 'padding-top',
            'units'    => 'px',
        ),
        array(
            'element'  => '.footer-widgets-wrap',
            'property' => 'padding-bottom',
            'units'    => 'px',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => '.footer-widgets-wrap',
            'property' => 'padding-top',
            'units'    => 'px',
            'function' => 'css',
        ),
        array(
            'element'  => '.footer-widgets-wrap',
            'property' => 'padding-bottom',
            'units'    => 'px',
            'function' => 'css',
        ),
    ),
    'choices'      => array(
        'min'  => 0,
        'max'  => 200,
        'step' => 1,
    )
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'footer_copy_padding',
    'label'       => esc_attr__( 'Footer Copy Padding', 'luxe-text-domain' ),
    'description' => esc_attr__( 'The space above and below your footer copy area.', 'luxe-text-domain' ),
    'section'     => 'footer',
    'default'     => '30',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.footer-copy',
            'property' => 'padding-top',
            'units'    => 'px',
        ),
        array(
            'element'  => '.footer-copy',
            'property' => 'padding-bottom',
            'units'    => 'px',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => '.footer-copy',
            'property' => 'padding-top',
            'units'    => 'px',
            'function' => 'css',
        ),
        array(
            'element'  => '.footer-copy',
            'property' => 'padding-bottom',
            'units'    => 'px',
            'function' => 'css',
        ),
    ),
    'choices'      => array(
        'min'  => 0,
        'max'  => 100,
        'step' => 1,
    )
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'dimension',
    'settings'    => 'footer_copy_border_width',
    'label'       => esc_attr__( 'Footer Copy Border Width', 'luxe-text-domain' ),
    'description' => esc_attr__( 'The width of the line between your footer copy and widgets/content.', 'luxe-text-domain' ),
    'section'     => 'footer',
    'default'     => '0px',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.footer-copy',
            'property' => 'border-top-width',
        ),
    ),
    'js_vars'     => array(
        array(
            'element'  => '.footer-copy',
            'property' => 'border-top-width',
            'function' => 'css',
        ),
    ),
    'choices' => array(
        'units' => array( 'px' )
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'footer_copy_border_color',
    'label'       => esc_attr__( 'Footer Copy Border Color', 'luxe-text-domain' ),
    'section'     => 'footer',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.footer-copy',
            'property' => 'border-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.footer-copy',
            'property' => 'border-color',
            'function' => 'css',
        ),
    ),
) );