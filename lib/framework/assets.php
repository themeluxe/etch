<?php

namespace Luxe\Assets;

use Luxe\Helper;

/**
 * Theme setup
 */
function luxe_editor_style() {
  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style(luxe_asset_path('styles/main.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\luxe_editor_style');

/**
 * Theme assets
 */
function luxe_assets() {
  wp_enqueue_style('luxe_main', luxe_asset_path('styles/main.css'), false, null);

  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  wp_enqueue_script('luxe_main', luxe_asset_path('scripts/main.js'), array('jquery'), null, true);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\luxe_assets', 100);

/**
 * Scroll snapping for rows
 */
function luxe_snap_rows() {
  $page_id = Helper\get_post_id();
  $snap_rows = get_post_meta( $page_id, '_page_layout_snap_rows', true );
  if ($snap_rows) { 
    ob_start();
    ?>
    <script>
         jQuery(document).ready(function($) {
           $.scrollify({
               section : ".outer-container",
           });
         });
    </script>
    <?php 
    $content = ob_get_clean();
    echo $content;
  }
}
add_action( 'wp_footer', __NAMESPACE__ . '\\luxe_snap_rows' );

/**
 * Get paths for assets
 */
class LuxeJsonManifest {
  private $manifest;

  public function __construct($manifest_path) {
    if (file_exists($manifest_path)) {
      ob_start();
      include $manifest_path;
      $file = ob_get_contents();
      ob_end_clean();
      $this->manifest = json_decode($file, true);
    } else {
      $this->manifest = array();
    }
  }

  public function get() {
    return $this->manifest;
  }

  public function getPath($key = '', $default = null) {
    $collection = $this->manifest;
    if (is_null($key)) {
      return $collection;
    }
    if (isset($collection[$key])) {
      return $collection[$key];
    }
    foreach (explode('.', $key) as $segment) {
      if (!isset($collection[$segment])) {
        return $default;
      } else {
        $collection = $collection[$segment];
      }
    }
    return $collection;
  }
}

function luxe_asset_path($filename) {
  $dist_path = get_template_directory_uri() . '/dist/';
  $directory = dirname($filename) . '/';
  $file = basename($filename);
  static $manifest;
  if (empty($manifest)) {
    $manifest_path = get_template_directory() . '/dist/' . 'assets.json';
    $manifest = new LuxeJsonManifest($manifest_path);
  }
  if (array_key_exists($file, $manifest->get())) {
    $manifest_array = $manifest->get();
    return $dist_path . $directory . $manifest_array[$file];
  } else {
    return $dist_path . $directory . $file;
  }
}
