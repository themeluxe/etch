<?php

namespace Luxe\Metaboxes;

use Luxe\Helper;
use Luxe\Assets;

/**
 * CMB2
 */
require_once LUXE_FRAMEWORK_VENDOR_PATH . 'WebDevStudios/cmb2/init.php';
require_once LUXE_FRAMEWORK_VENDOR_PATH . 'JayWood/CMB2_RGBa_Picker/jw-cmb2-rgba-colorpicker.php';

/**
 * Fix cmb2 asset URL
 */
function update_cmb2_url( $url ) {
    return LUXE_FRAMEWORK_VENDOR_URL . '/WebDevStudios/cmb2/';
}
add_filter( 'cmb2_meta_box_url', __NAMESPACE__ . '\\update_cmb2_url' );

/**
 * Override rgba plugin script path
 */
function metabox_admin_scripts() {
    wp_dequeue_script('jw-cmb2-rgba-picker-js');
    $screen = get_current_screen();
    if ( !empty($screen->post_type) ) {
        wp_enqueue_script( 'jw-cmb2-rgba-picker-js-2', get_template_directory_uri() . '/lib/framework/vendor/JayWood/CMB2_RGBa_Picker/js/jw-cmb2-rgba-picker.js', array( 'wp-color-picker' ), null, true );
    }
}
add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\\metabox_admin_scripts', 1000 );


/**
 * Render CSS
 */
function render_css() {
    $page_id = Helper\get_post_id();
    $bg_image = get_post_meta( $page_id, '_page_header_bg_image', true );
    if ( is_singular( 'post' ) && $bg_image == '' ) {
        $bg_image = wp_get_attachment_image_url( get_post_thumbnail_id( $page_id ), 'full' );
    }
    $bg_color = get_post_meta( $page_id, '_page_header_bg_color', true );
    $bg_attachment = get_post_meta( $page_id, '_page_header_bg_attachment', true );
    $bg_size = get_post_meta( $page_id, '_page_header_bg_size', true );
    $padding_top = get_post_meta( $page_id, '_page_header_padding_top', true );
    $padding_bottom = get_post_meta( $page_id, '_page_header_padding_bottom', true );
    $typography_color = get_post_meta( $page_id, '_page_header_typography_color', true );
    $overlay_color = get_post_meta( $page_id, '_page_header_overlay_color', true );
    ?>
    <style id="theme-metabox-css" type="text/css">
        .page-header {
            <?php if(!empty($bg_image) && $bg_image != 'theme-default') : ?>
                background-image: url('<?php echo esc_url($bg_image); ?>');<?php endif; ?>
            <?php if(!empty($bg_color) && $bg_color != 'theme-default') : ?>
                background-color: <?php echo esc_attr($bg_color); ?>;
            <?php endif; ?>
            <?php if(!empty($bg_attachment) && $bg_attachment != 'theme-default') : ?>
                background-attachment: <?php echo esc_attr($bg_attachment); ?>;
            <?php endif; ?>
            <?php if(!empty($bg_size) && $bg_size != 'theme-default') : ?>
                background-size: <?php echo esc_attr($bg_size); ?>;
            <?php endif; ?>
            <?php if(!empty($padding_top) && $padding_top != 'theme-default') : ?>
                padding-top: <?php echo esc_attr($padding_top); ?>;
            <?php endif; ?>
            <?php if(!empty($padding_bottom) && $padding_bottom != 'theme-default') : ?>
                padding-bottom: <?php echo esc_attr($padding_bottom); ?>;
            <?php endif; ?>
            <?php if(!empty($typography_color) && $typography_color != 'theme-default') : ?>
                color: <?php echo esc_attr($typography_color); ?>;
            <?php endif; ?>
        }
        .page-header .page-header-overlay {
            <?php if(!empty($overlay_color)) : ?>
                background-color: <?php echo esc_attr($overlay_color); ?>;
            <?php endif; ?>
        }
        .page-header h1, .page-header .subtitle {
            <?php if(!empty($typography_color) && $typography_color != 'theme-default') : ?>
                color: <?php echo esc_attr($typography_color); ?>;
            <?php endif; ?>
        }
    </style>
    <?php
}
add_action('wp_head', __NAMESPACE__ . '\\render_css');

/**
 * Post Content Formats
 */
function portfolio_content() {
    $prefix = '_portfolio_content_';

    $post_content = new_cmb2_box( array(
        'id'            => $prefix . 'metabox',
        'title'         => esc_attr__( 'Portfolio Content', 'luxe-text-domain' ),
        'object_types'  => array( 'portfolio' ),
        // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
    ) );

    $post_content->add_field( array(
        'id'   => $prefix . 'custom',
        'name' => 'Custom Content',
        'desc' => 'Insert any content you\'d like to use on your single portfolio item page here.  This will not show up in the grid, but instead replace any content on the portfolio item page.',
        'type' => 'wysiwyg'
    ) );
    
}
add_action( 'cmb2_admin_init', __NAMESPACE__ . '\\portfolio_content' );

/**
 * Post Content Formats
 */
function post_content() {
    $prefix = '_post_content_';

    $post_content = new_cmb2_box( array(
        'id'            => $prefix . 'metabox',
        'title'         => esc_attr__( 'Post Content', 'luxe-text-domain' ),
        'object_types'  => array( 'post', 'portfolio' ),
        // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
    ) );

    $gallery_images = $post_content->add_field( array(
        'id'          => $prefix . 'gallery_images',
        'type'        => 'group',
        'description' => esc_attr__( 'Gallery Images', 'luxe-text-domain' ),
        'options'     => array(
            'group_title'   => esc_attr__( 'Image {#}', 'luxe-text-domain' ), // {#} gets replaced by row number
            'add_button'    => esc_attr__( 'Add Another Image', 'luxe-text-domain' ),
            'remove_button' => esc_attr__( 'Remove Image', 'luxe-text-domain' ),
            'sortable'      => true, // beta
            // 'closed'     => true, // true to have the groups closed by default
        ),
    ) );

    $post_content->add_group_field( $gallery_images, array(
        'name' => esc_attr__( 'Gallery Image', 'luxe-text-domain' ),
        'id'   => 'image',
        'type' => 'file',
    ) );

    $post_content->add_field( array(
        'id'   => $prefix . 'video',
        'name' => 'Video Iframe',
        'desc' => 'Insert your video iframe or other code here.  Make sure you have a featured image set to act as the placeholder.',
        'type' => 'textarea_code'
    ) );

    $post_content->add_field( array(
        'name' => esc_attr__( 'Audio File', 'luxe-text-domain' ),
        'id'   => $prefix . 'audio',
        'desc' => 'Add your audio file here.',
        'type' => 'file',
    ) );

    $post_content->add_field( array(
        'name' => esc_attr__( 'Link', 'luxe-text-domain' ),
        'desc' => esc_attr__( 'Add a URL for this post to link to.', 'luxe-text-domain' ),
        'id'   => $prefix . 'link',
        'type' => 'text_url',
    ) );
    
    $post_content->add_field( array(
        'name'             => esc_attr__( 'Grid Item Size', 'luxe-text-domain' ),
        'desc'             => esc_attr__( '', 'luxe-text-domain' ),
        'id'               => $prefix . 'grid_item_size',
        'type'             => 'select',
        'default'          => '',
        'desc' => esc_attr__( 'This size will be used in the grid if using the flexible layout option.', 'luxe-text-domain' ),
        'options'          => array(
            '' => esc_attr__( 'Default', 'luxe-text-domain' ),
            'grid-item-small'   => esc_attr__( 'Small', 'luxe-text-domain' ),
            'grid-item-medium'   => esc_attr__( 'Medium', 'luxe-text-domain' ),
            'grid-item-large'   => esc_attr__( 'Large', 'luxe-text-domain' ),
        ),
    ) );
    
}
add_action( 'cmb2_admin_init', __NAMESPACE__ . '\\post_content' );


/**
 * Page Header
 */
function page_header() {
    $prefix = '_page_header_';

    $page_header = new_cmb2_box( array(
        'id'            => $prefix . 'metabox',
        'title'         => esc_attr__( 'Page Header', 'luxe-text-domain' ),
        'object_types'  => array( 'page', 'post' ),
        // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
    ) );

    $page_header->add_field( array(
        'name'             => esc_attr__( 'Show Page Header', 'luxe-text-domain' ),
        'desc'             => esc_attr__( 'Override page header and title settings.', 'luxe-text-domain' ),
        'id'               => $prefix . 'display',
        'type'             => 'select',
        'default'          => 'theme-default',
        'options'          => array(
            'theme-default' => esc_attr__( 'Theme Default', 'luxe-text-domain' ),
            'show'   => esc_attr__( 'Show', 'luxe-text-domain' ),
            'hide'   => esc_attr__( 'Hide', 'luxe-text-domain' ),
        ),
    ) );
    $page_header->add_field( array(
        'name' => esc_attr__( 'Override Page Title', 'luxe-text-domain' ),
        'desc' => esc_attr__( 'Enter text other than the default page title if you want to display other text here.', 'luxe-text-domain' ),
        'id'   => $prefix . 'page_title',
        'type' => 'text',
    ) );
    $page_header->add_field( array(
        'name' => esc_attr__( 'Page Subtitle', 'luxe-text-domain' ),
        'desc' => esc_attr__( 'Text below your page\'s title.', 'luxe-text-domain' ),
        'id'   => $prefix . 'page_subtitle',
        'type' => 'text',
    ) );
    $page_header->add_field( array(
        'name'    => esc_attr__( 'Font Color', 'luxe-text-domain' ),
        'id'      => $prefix . 'typography_color',
        'type'    => 'colorpicker',
        'default' => '',
    ) );
    $page_header->add_field( array(
        'name' => esc_attr__( 'Background Image', 'luxe-text-domain' ),
        'desc' => esc_attr__( 'Upload an image or enter a URL.', 'luxe-text-domain' ),
        'id'   => $prefix . 'bg_image',
        'type' => 'file',
    ) );
    $page_header->add_field( array(
        'name'    => esc_attr__( 'Background Color', 'luxe-text-domain' ),
        'id'      => $prefix . 'bg_color',
        'type'    => 'colorpicker',
        'default' => '',
    ) );
    $page_header->add_field( array(
        'name'             => esc_attr__( 'Background Attachment', 'luxe-text-domain' ),
        'desc'             => esc_attr__( '', 'luxe-text-domain' ),
        'id'               => $prefix . 'bg_attachment',
        'type'             => 'select',
        'default'          => 'theme-default',
        'options'          => array(
            'theme-default' => esc_attr__( 'Theme Default', 'luxe-text-domain' ),
            'scroll'   => esc_attr__( 'Scroll', 'luxe-text-domain' ),
            'fixed'   => esc_attr__( 'Fixed', 'luxe-text-domain' ),
        ),
    ) );
    $page_header->add_field( array(
        'name'             => esc_attr__( 'Background Size', 'luxe-text-domain' ),
        'desc'             => esc_attr__( '', 'luxe-text-domain' ),
        'id'               => $prefix . 'bg_size',
        'type'             => 'select',
        'default'          => 'theme-default',
        'options'          => array(
            'theme-default' => esc_attr__( 'Theme Default', 'luxe-text-domain' ),
            'auto'   => esc_attr__( 'Auto', 'luxe-text-domain' ),
            'cover'   => esc_attr__( 'Cover (stretch)', 'luxe-text-domain' ),
            'contain'   => esc_attr__( 'Contain', 'luxe-text-domain' ),
        ),
    ) );
    $page_header->add_field( array(
        'name' => esc_attr__( 'Padding Top', 'luxe-text-domain' ),
        'desc' => esc_attr__( 'Padding above page header.  You can use percentage or pixels here (e.g., 300px or 8%).', 'luxe-text-domain' ),
        'id'   => $prefix . 'padding_top',
        'type' => 'text',
    ) );
    $page_header->add_field( array(
        'name' => esc_attr__( 'Padding Bottom', 'luxe-text-domain' ),
        'desc' => esc_attr__( 'Padding below page header.  You can use percentage or pixels here (e.g., 300px or 8%).', 'luxe-text-domain' ),
        'id'   => $prefix . 'padding_bottom',
        'type' => 'text',
    ) );
    $page_header->add_field( array(
        'name'    => esc_attr__( 'Overlay Color', 'luxe-text-domain' ),
        'id'      => $prefix . 'overlay_color',
        'type'    => 'rgba_colorpicker',
        'default' => '',
    ) );
    $page_header->add_field( array(
        'name' => esc_attr__( 'Full Browser Height', 'luxe-text-domain' ),
        'desc' => esc_attr__( 'Stretch the page header to make it full browser height.', 'luxe-text-domain' ),
        'id'   => $prefix . 'full_browser_height',
        'type' => 'checkbox',
    ) );

}
add_action( 'cmb2_admin_init', __NAMESPACE__ . '\\page_header' );

/**
 * Page layout
 */
function page_layout() {
    $prefix = '_page_layout_';

    $page_layout = new_cmb2_box( array(
        'id'            => $prefix . 'metabox',
        'title'         => esc_attr__( 'Page Layout', 'luxe-text-domain' ),
        'object_types'  => array( 'page', 'post' ),
        // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
    ) );
    $page_layout->add_field( array(
        'name'             => esc_attr__( 'Header Scheme', 'luxe-text-domain' ),
        'desc'             => esc_attr__( '', 'luxe-text-domain' ),
        'id'               => $prefix . 'header_scheme',
        'type'             => 'select',
        'default'          => 'theme-default',
        'desc' => esc_attr__( 'Set these color schemes in the customizer under their respective tabs.', 'luxe-text-domain' ),
        'options'          => array(
            'theme-default' => esc_attr__( 'Theme Default', 'luxe-text-domain' ),
            'header-dark-active'   => esc_attr__( 'Dark Header', 'luxe-text-domain' ),
            'header-light-active'   => esc_attr__( 'Light Header', 'luxe-text-domain' ),
        ),
    ) );
    $page_layout->add_field( array(
        'name'             => esc_attr__( 'Show Sidebar', 'luxe-text-domain' ),
        'desc'             => esc_attr__( '', 'luxe-text-domain' ),
        'id'               => $prefix . 'sidebar',
        'type'             => 'select',
        'default'          => 'theme-default',
        'options'          => array(
            'theme-default' => esc_attr__( 'Theme Default', 'luxe-text-domain' ),
            'show'   => esc_attr__( 'Show', 'luxe-text-domain' ),
            'hide'   => esc_attr__( 'Hide', 'luxe-text-domain' ),
        ),
    ) );
    $page_layout->add_field( array(
        'name' => 'Snap Rows',
        'desc' => 'If enabled your visual composer rows will snap to the screen when scrolled to.',
        'id'   => $prefix . 'snap_rows',
        'type' => 'checkbox'
    ) );

}
add_action( 'cmb2_admin_init', __NAMESPACE__ . '\\page_layout' );



/**
 * Hook in and add a metabox to add fields to the user profile pages
 */
function user_profile() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_user_';

    /**
     * Metabox for the user profile screen
     */
    $cmb_user = new_cmb2_box( array(
        'id'               => $prefix . 'edit',
        'title'            => esc_attr__( 'User Profile Metabox', 'luxe-text-domain' ),
        'object_types'     => array( 'user' ),
        'show_names'       => true,
        'new_user_section' => 'add-new-user',
    ) );

    $cmb_user->add_field( array(
        'name'     => esc_attr__( 'Extra Info', 'luxe-text-domain' ),
        'desc'     => esc_attr__( '', 'luxe-text-domain' ),
        'id'       => $prefix . 'extra_info',
        'type'     => 'title',
        'on_front' => false,
    ) );

    $cmb_user->add_field( array(
        'name'    => esc_attr__( 'Avatar', 'luxe-text-domain' ),
        'desc'    => esc_attr__( '', 'luxe-text-domain' ),
        'id'      => $prefix . 'avatar',
        'type'    => 'file',
    ) );

    $cmb_user->add_field( array(
        'name' => esc_attr__( 'Facebook URL', 'luxe-text-domain' ),
        'desc' => esc_attr__( '', 'luxe-text-domain' ),
        'id'   => $prefix . 'facebook_url',
        'type' => 'text_url',
    ) );

    $cmb_user->add_field( array(
        'name' => esc_attr__( 'Twitter URL', 'luxe-text-domain' ),
        'desc' => esc_attr__( '', 'luxe-text-domain' ),
        'id'   => $prefix . 'twitter_url',
        'type' => 'text_url',
    ) );

    $cmb_user->add_field( array(
        'name' => esc_attr__( 'Google+ URL', 'luxe-text-domain' ),
        'desc' => esc_attr__( '', 'luxe-text-domain' ),
        'id'   => $prefix . 'googleplus_url',
        'type' => 'text_url',
    ) );

    $cmb_user->add_field( array(
        'name' => esc_attr__( 'Linkedin URL', 'luxe-text-domain' ),
        'desc' => esc_attr__( '', 'luxe-text-domain' ),
        'id'   => $prefix . 'linkedin_url',
        'type' => 'text_url',
    ) );

}
add_action( 'cmb2_admin_init', __NAMESPACE__ . '\\user_profile' );

