<?php

/**
 * Luxe Framework Init
 *
 * The $luxe_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 */

define('LUXE_FRAMEWORK_PATH', trailingslashit(get_template_directory()) . 'lib/framework/');
define('LUXE_FRAMEWORK_URL', trailingslashit(get_template_directory_uri()) . 'lib/framework/');
define('LUXE_FRAMEWORK_VENDOR_PATH', LUXE_FRAMEWORK_PATH . 'vendor/' );
define('LUXE_FRAMEWORK_VENDOR_URL', LUXE_FRAMEWORK_URL . 'vendor/' );

require_once( LUXE_FRAMEWORK_PATH . 'helper.php'); // Helper
require_once( LUXE_FRAMEWORK_PATH . 'assets.php'); // Scripts and stylesheets
require_once( LUXE_FRAMEWORK_PATH . 'extras.php'); // Custom functions
require_once( LUXE_FRAMEWORK_PATH . 'setup.php'); // Theme setup
require_once( LUXE_FRAMEWORK_PATH . 'plugins.php'); // Theme setup
require_once( LUXE_FRAMEWORK_PATH . 'elements.php'); // Page elements
require_once( LUXE_FRAMEWORK_PATH . 'wrapper.php'); // Theme wrapper class
require_once( LUXE_FRAMEWORK_PATH . 'customizer.php'); // Theme customizer
require_once( LUXE_FRAMEWORK_PATH . 'header.php'); // Headers
require_once( LUXE_FRAMEWORK_PATH . 'options.php'); // Options
require_once( LUXE_FRAMEWORK_PATH . 'metaboxes.php'); // Metaboxes
require_once( LUXE_FRAMEWORK_PATH . 'woocommerce.php'); // Woocommerce
require_once( LUXE_FRAMEWORK_PATH . 'importer.php'); // Importer for demo content