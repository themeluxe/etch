<?php

/**
 * Luxe framework setup
 *
 * The $luxe_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 */
define('ETCH_LIB_DIR', trailingslashit(get_template_directory()) . 'lib/');


// Check for ThemeLuxe Framework
//require_once( ETCH_LIB_DIR . 'plugins.php');

require_once( ETCH_LIB_DIR . 'framework/init.php');
require_once( ETCH_LIB_DIR . 'plugins.php');
require_once( ETCH_LIB_DIR . 'custom-post-types.php');
require_once( ETCH_LIB_DIR . 'shortcodes.php');
require_once( ETCH_LIB_DIR . 'importer.php');
require_once( ETCH_LIB_DIR . 'metaboxes.php');
require_once( ETCH_LIB_DIR . 'woocommerce.php');
